#coding: utf-8
from django.contrib import admin

from .models import Student, StudentGroup, Event, Subject, GradeCategory, Grade
# Register your models here.


admin.site.register([StudentGroup, Event, Subject, GradeCategory, Grade])
#admin.site.register(Student, StudentAdmin )

class MyAdmin(admin.ModelAdmin):
	action_on_bottom = True
	save_on_top = True


class GradeInline(admin.TabularInline):
	model = Grade
	extra = 1
	#max_num = 3
	


class StudentAdmin(MyAdmin):
	list_display = ['first_name', 'last_name', 'identity', 'email']
	list_filter = ['group']
	list_editable = ['identity',  'email']
	search_fields = ['last_name','group__name']
	ordering = ['last_name']
	inlines = [GradeInline,]
	

admin.site.register(Student,StudentAdmin)	