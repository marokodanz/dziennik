# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'StudentGroup'
        db.create_table(u'zaliczenie_studentgroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'zaliczenie', ['StudentGroup'])

        # Adding model 'Student'
        db.create_table(u'zaliczenie_student', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=80, db_index=True)),
            ('identity', self.gf('django.db.models.fields.CharField')(max_length=20, db_index=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['zaliczenie.StudentGroup'])),
        ))
        db.send_create_signal(u'zaliczenie', ['Student'])

        # Adding model 'Subject'
        db.create_table(u'zaliczenie_subject', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'zaliczenie', ['Subject'])

        # Adding M2M table for field groups on 'Subject'
        m2m_table_name = db.shorten_name(u'zaliczenie_subject_groups')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('subject', models.ForeignKey(orm[u'zaliczenie.subject'], null=False)),
            ('studentgroup', models.ForeignKey(orm[u'zaliczenie.studentgroup'], null=False))
        ))
        db.create_unique(m2m_table_name, ['subject_id', 'studentgroup_id'])

        # Adding model 'Event'
        db.create_table(u'zaliczenie_event', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['zaliczenie.Subject'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'zaliczenie', ['Event'])

        # Adding M2M table for field students on 'Event'
        m2m_table_name = db.shorten_name(u'zaliczenie_event_students')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('event', models.ForeignKey(orm[u'zaliczenie.event'], null=False)),
            ('student', models.ForeignKey(orm[u'zaliczenie.student'], null=False))
        ))
        db.create_unique(m2m_table_name, ['event_id', 'student_id'])

        # Adding model 'GradeCategory'
        db.create_table(u'zaliczenie_gradecategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('max_value', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'zaliczenie', ['GradeCategory'])

        # Adding model 'Grade'
        db.create_table(u'zaliczenie_grade', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['zaliczenie.GradeCategory'])),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['zaliczenie.Student'])),
            ('subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['zaliczenie.Subject'])),
        ))
        db.send_create_signal(u'zaliczenie', ['Grade'])


    def backwards(self, orm):
        # Deleting model 'StudentGroup'
        db.delete_table(u'zaliczenie_studentgroup')

        # Deleting model 'Student'
        db.delete_table(u'zaliczenie_student')

        # Deleting model 'Subject'
        db.delete_table(u'zaliczenie_subject')

        # Removing M2M table for field groups on 'Subject'
        db.delete_table(db.shorten_name(u'zaliczenie_subject_groups'))

        # Deleting model 'Event'
        db.delete_table(u'zaliczenie_event')

        # Removing M2M table for field students on 'Event'
        db.delete_table(db.shorten_name(u'zaliczenie_event_students'))

        # Deleting model 'GradeCategory'
        db.delete_table(u'zaliczenie_gradecategory')

        # Deleting model 'Grade'
        db.delete_table(u'zaliczenie_grade')


    models = {
        u'zaliczenie.event': {
            'Meta': {'object_name': 'Event'},
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'students': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['zaliczenie.Student']", 'symmetrical': 'False'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['zaliczenie.Subject']"})
        },
        u'zaliczenie.grade': {
            'Meta': {'object_name': 'Grade'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['zaliczenie.GradeCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['zaliczenie.Student']"}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['zaliczenie.Subject']"}),
            'value': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'zaliczenie.gradecategory': {
            'Meta': {'object_name': 'GradeCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_value': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'zaliczenie.student': {
            'Meta': {'object_name': 'Student'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['zaliczenie.StudentGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identity': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_index': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'db_index': 'True'})
        },
        u'zaliczenie.studentgroup': {
            'Meta': {'object_name': 'StudentGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'zaliczenie.subject': {
            'Meta': {'object_name': 'Subject'},
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['zaliczenie.StudentGroup']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['zaliczenie']